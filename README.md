# Pico Project Generator カスタマイズ版

この pico-project-genetator は、本家の　pico-project-genetator に対して WildDuck の Mac 環境用にカスタマイズしたものです。
本家のリポジトリは[こちら](https://github.com/raspberrypi/pico-project-generator)です。

## 使用方法
```
$ export SDK_PICO_PATH=(pico-sdkへのフルパス)
$ /pico-project-generator/pico_project.py --gui
```
pico-project-generator の使用方法の詳細は本家を参照ください。

## 修正内容
* .vscode に配置されれる c_cpp_properties.json、extensions.json、launch.json、settings.json を手元の環境に合わせて修正
* openocd を起動するスクリプト openocd.sh を用意
